package com.controlspending.personal.spends.service;

import com.controlspending.personal.spends.Spends;

import java.util.List;

public interface IFindAllSpendsByIdPerson {

    List<Spends>  findAllSpendsByIdPerson(Long id);
}
