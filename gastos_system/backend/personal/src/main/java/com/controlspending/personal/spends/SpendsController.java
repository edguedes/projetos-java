package com.controlspending.personal.spends;

import com.controlspending.personal.spends.dto.SpendsCreateDTO;
import com.controlspending.personal.spends.service.ISpendsCreateService;
import com.controlspending.personal.spends.service.ISpendsFindById;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/spends")
@RequiredArgsConstructor
public class SpendsController {

    private final ISpendsCreateService spendsCreateService;
    private final ISpendsFindById spendsFindByIdService;
    private final ISpendsRepository spendsRepository;

    @PostMapping(value = "/{idPerson}")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@Valid @RequestBody SpendsCreateDTO spendsDTO, @PathVariable Long idPerson) {
        spendsCreateService.create(spendsDTO, idPerson); }

    @GetMapping(value = "/find-all/{idPerson}")
    @ResponseStatus(HttpStatus.OK)
    public List<Spends> findAllPerson(@PathVariable Long idPerson) {
        return spendsRepository.findByPessoaId(idPerson); }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Spends findPerson(@PathVariable Long id) { return spendsFindByIdService.findSpendById(id);
    }
}
