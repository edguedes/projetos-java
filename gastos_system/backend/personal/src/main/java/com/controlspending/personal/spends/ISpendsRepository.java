package com.controlspending.personal.spends;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ISpendsRepository extends JpaRepository<Spends, Long> {

    List<Spends> findByPessoaId(Long id);
}
