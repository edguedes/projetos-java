package com.controlspending.personal.person.service;

import com.controlspending.personal.person.Person;
import com.controlspending.personal.person.dto.PersonDTO;

public interface IPersonCreateService {

    PersonDTO create(PersonDTO personDTO);
}
