package com.controlspending.personal.spends.service.implematetion;

import com.controlspending.personal.exception.NotFoundException;
import com.controlspending.personal.person.Person;
import com.controlspending.personal.person.service.implementation.PersonService;
import com.controlspending.personal.spends.ISpendsRepository;
import com.controlspending.personal.spends.Spends;
import com.controlspending.personal.spends.dto.SpendsCreateDTO;
import com.controlspending.personal.spends.service.IFindAllSpendsByIdPerson;
import com.controlspending.personal.spends.service.ISpendsCreateService;
import com.controlspending.personal.spends.service.ISpendsFindById;
import com.controlspending.personal.util.RemoveSpecialCharacters;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SpendsService implements ISpendsCreateService, ISpendsFindById, IFindAllSpendsByIdPerson {

    private final PersonService personFindByIdService;

    private final ISpendsRepository spendsRepository;

    @Override
    public void create(SpendsCreateDTO spendsDTO, Long idPerson) {
        Person person = personFindByIdService.findPersonById(idPerson);
        Spends spends = spendsDTO.parseToSpends();

        spends.setPessoa(person);
        spends.setDescricao(RemoveSpecialCharacters.validate(spends.getDescricao()));
        spends.setDataTransacao(LocalDateTime.now());
        spendsRepository.save(spends);
    }

    @Override
    public Spends findSpendById(Long id) {
        return spendsRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(id.toString()));
    }

    @Override
    public List<Spends> findAllSpendsByIdPerson(Long id) {
        return spendsRepository.findByPessoaId(id);
    }
}
