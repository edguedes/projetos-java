package com.controlspending.personal.person;

import com.controlspending.personal.person.dto.PersonDTO;
import com.controlspending.personal.person.service.IPersonCreateService;
import com.controlspending.personal.person.service.IPersonFindAll;
import com.controlspending.personal.person.service.IPersonFindByIdService;
import com.controlspending.personal.person.service.IPersonFindTokenService;
import com.controlspending.personal.person.service.implementation.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/persons")
@RequiredArgsConstructor
class PersonController {

    private final PersonService personService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public PersonDTO create(@Valid @RequestBody PersonDTO personDTO) { return personService.create(personDTO); }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public Page<Person> findAllPerson(Pageable pageable) { return personService.findPersonAll(pageable); }

    @GetMapping(value = "/token/{token}")
    @ResponseStatus(HttpStatus.OK)
    public Person findPersonToken(@PathVariable String token) {
        return personService.findPersonToken(token);
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Person findPersonId(@PathVariable Long id) {
        return personService.findPersonById(id);
    }
}
