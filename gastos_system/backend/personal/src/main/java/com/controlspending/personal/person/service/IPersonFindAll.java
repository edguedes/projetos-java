package com.controlspending.personal.person.service;

import com.controlspending.personal.person.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IPersonFindAll {

    Page<Person> findPersonAll(Pageable pageable);
}
