package com.controlspending.personal.person.service.implementation;

import com.controlspending.personal.exception.NotFoundException;
import com.controlspending.personal.person.IPersonRepository;
import com.controlspending.personal.person.Person;
import com.controlspending.personal.person.dto.PersonDTO;
import com.controlspending.personal.person.service.IPersonCreateService;
import com.controlspending.personal.person.service.IPersonFindAll;
import com.controlspending.personal.person.service.IPersonFindByIdService;
import com.controlspending.personal.person.service.IPersonFindTokenService;
import com.controlspending.personal.util.RemoveSpecialCharacters;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
//@RequiredArgsConstructor
@AllArgsConstructor
public class PersonService implements IPersonCreateService, IPersonFindByIdService, IPersonFindTokenService, IPersonFindAll {

    private final IPersonRepository personRepository;

    @Override
    public PersonDTO create(PersonDTO personDTO) {
        Person person = personDTO.parseToPerson();
        person.setToken(UUID.randomUUID().toString());
        person.setName(RemoveSpecialCharacters.validate(person.getName()));
        return personRepository.save(person).parseToPersonDTO();
    }

    @Override
    public Person findPersonById(Long id) {
        return personRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(id.toString()));
    }

    @Override
    public Person findPersonToken(String token) {
        return personRepository.findByToken(token)
                .orElseThrow(() -> new NotFoundException(token));
    }

    @Override
    public Page<Person> findPersonAll(Pageable pageable) {
        return personRepository.findAll(pageable);
    }
}
