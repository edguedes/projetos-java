package com.controlspending.personal.person;

import com.controlspending.personal.person.dto.PersonDTO;
import com.controlspending.personal.person.service.implementation.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
public class PersonControllerTest {

    @Autowired
    PersonService personCreateService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testCriandoPessoa() throws Exception{
        PersonDTO personDTO = new PersonDTO();
        personDTO.setName("Maria Joaquina");

        Person person = new Person();
        person.setName(personDTO.getName());

        when(personCreateService.create(any(PersonDTO.class))).thenReturn(personDTO);

        mockMvc.perform(get("/api/persons")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }
}
